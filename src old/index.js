import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import App from "./composant/App";

ReactDOM.render(<App/>,document.querySelector('#root'))
import React, {Component} from "react";
import Button from "./Button";


const Membre = ({nom, age, viellir, rajeunir, changeNom}) => {
    return(
        <div className="card mr-2" style={{width:'150px'}}>
            <div className="card-body">
              <h4 className="card-title">{nom}</h4>
              <p>{age}</p>
                <p>
                    <input placeholder={nom}
                           onChange={changeNom}
                           type="text"
                           style={{width:"125px"}} />
                </p>
                <Button lib={"+2 ans"} btnAction={viellir}/>
                <Button lib={"-2 ans"} btnAction={rajeunir}/>
            </div>
        </div>
    )
}

export default Membre;
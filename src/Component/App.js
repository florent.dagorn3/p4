import React,{Fragment,Component} from "react";
import Header from "./Header";
import famille  from "../data/familles";
import Membre from "./Membre";
import  Button  from "./Button";


export default class App extends Component {
    state = {
        famille 
    }

    handleClick = (nbr) => {
        const famille = {...this.state.famille}
        famille.membre3.age += nbr;
        this.setState({famille})
        //this.setState({famille:{membre3:{age: this.state.famille.membre3.age+nbr}}})
    }

    handleChange =(nbr, sens, id) => {
        const famille = {...this.state.famille};
        (sens == "plus")?(famille[id].age += nbr):(famille[id].age -= nbr);
        this.setState({famille})
    }

    handleChangeNom = (e, id) => {
    const famille = {...this.state.famille};
    famille[id].nom = e.target.value
        this.setState({famille})
    }

    render(){
        const listeMembre = Object.keys(this.state.famille).map( membre => (
        <Membre key ={membre}
                nom={this.state.famille[membre].nom}
                age={this.state.famille[membre].age}
                viellir={() => this.handleChange(3, "plus", membre)}
                rajeunir={() => this.handleChange(3, "moins", membre)}
                changeNom={ e=> this.handleChangeNom(e, membre) }
        />
        ));
        return(
       <Fragment>
           <Header/>
          <div className="d-flex flex-row flex-wrap justify-content-start">
              {listeMembre}
          </div>
          <Button lib={"a que coucou"} btnAction={ () => alert("coucou")}/>
           <Button lib={"+2 ans"} btnAction={() => this.handleClick(2)}/>
           <Button lib={"-2 ans"} btnAction={() => this.handleClick(-2)}/>
       </Fragment>

        )
    }
}
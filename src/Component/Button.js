import React from "react";

const Button = ({btnAction, lib}) => {
  return (
      <button onClick={btnAction}
              className="btn btn-sm btn-outline-primary mt-3 mb-3">
        {lib}
      </button>
  )
}

export default Button;